# Jupyter Notebook proxied with Nginx on Ubuntu

![scheme_Nginx-Jupyter-Docker-Ubuntu.png](./scheme_Nginx-Jupyter-Docker-Ubuntu.png)

## Files in this repository

 * `Dockerfile` -- docker configuration, runs `CMD ./nginx-and-jupyter.sh` at `docker run`
 * `nginx-and-jupyter.sh` -- starts `nginx` service and `jupyter notebook` server
 * `jupyter-notebook.conf` -- `nginx` configuration
 * `jupyter_notebook_config.py` -- Jupyter Notebook configuration file

## Setting up and debugging the dockerized services

### Building the image

```bash
docker build . -t nginx-jupyter-ubuntu
```

### Running the image

```bash
docker run -p 8080:80 --name nginx-jupyter-ubuntu nginx-jupyter-ubuntu
```

where:
 * 8080 is the port exposed outside of the docker image (on a host machine)
 * 80 is the TCP port inside the container on which nginx proxies

### Entering the image interactively

```bash
docker exec -it nginx-jupyter-ubuntu /bin/zsh
```

### Cleanup

```bash
docker stop $(docker ps -a | grep 'nginx-jupyter-ubuntu' | awk '{ print $1 }') && docker rm $(docker ps -a | grep 'nginx-jupyter-ubuntu' | awk '{ print $1 }') && docker rmi nginx-jupyter-ubuntu
```

